﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardConstants
{
    public const int NumBasicShapes = 6;

    public const int BombShapeNum = 6;
    public const int FreezeShapeNum = 7;
}
