﻿using System;

public enum BonusTypeEnum
{
    None,
    Bomb,
    Freeze
}

public enum GameStateEnum
{
    Idle,
    Animating
}