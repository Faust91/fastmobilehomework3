﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private static AudioController _instance;

    public static AudioController Instance
    {
        get { return _instance; }
    }

    private AudioSource _source;

    [SerializeField]
    private AudioClip _swapSound;
    [SerializeField]
    private AudioClip _coinSound;
    [SerializeField]
    private AudioClip _bombSound;
    [SerializeField]
    private AudioClip _freezeSound;

    private void Awake()
    {
        // Only 1 Game Manager can exist at a time
        if (_instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            _instance = GetComponent<AudioController>();
            _source = this.GetComponent<AudioSource>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlaySwapSound()
    {
        if (_source.isPlaying)
        {
            _source.Stop();
        }
        _source.clip = _swapSound;
        _source.Play();
    }

    public void PlayCoinSound()
    {
        if (_source.isPlaying)
        {
            _source.Stop();
        }
        _source.clip = _coinSound;
        _source.Play();
    }

    public void PlayBombSound()
    {
        if (_source.isPlaying)
        {
            _source.Stop();
        }
        _source.clip = _bombSound;
        _source.Play();
    }

    public void PlayFreezeSound()
    {
        if (_source.isPlaying)
        {
            _source.Stop();
        }
        _source.clip = _freezeSound;
        _source.Play();
    }
}
