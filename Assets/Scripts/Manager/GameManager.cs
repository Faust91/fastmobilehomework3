﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [System.Serializable]
    public class RatingCustomEvent : UnityEvent<int> { }

    private static GameManager _instance;

    private BoardController _boardController;

    private int _rating;

    [SerializeField]
    private GameObject _fadeToBlack;

    [SerializeField]
    private string _mainSceneName = "MainScene";

    [Header("Panels")]
    [SerializeField]
    private GameObject _pausePanel;
    [SerializeField]
    private GameObject _gameOverPanel;

    [Header("Listeners")]
    public RatingCustomEvent NewRatingEvent;
    

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        // Only 1 Game Manager can exist at a time
        if (_instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            _instance = GetComponent<GameManager>();
        }
        else
        {
            Destroy(gameObject);
        }

        _rating = 0;
    }

    private void Start()
    {
        _boardController = BoardController.Instance;
    }

    public BonusTypeEnum RetrievePowerUp()
    {
        return Settings.Instance.GetPowerUp();
    }

    public void IncrementRating(int addValue)
    {
        _rating += addValue;
        NewRatingEvent?.Invoke(_rating);
    }

    public void FreezeTimeFor(int time)
    {
        gameObject.GetComponent<TimerController>().FreezeFor(time);
    }

    private void pauseGame()
    {
        timeFlowing(false);
        _boardController.Pause();
    }

    private void resumeGame()
    {
        timeFlowing(true);
        _boardController.Resume();
    }

    public void Pause()
    {
        pauseGame();
        _pausePanel.SetActive(true);
    }

    public void Resume()
    {
        resumeGame();
        _pausePanel.SetActive(false);
    }

    private void timeFlowing(bool flow)
    {
        if (flow)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }
    }

    public void GameOver()
    {
        pauseGame();
        DataRepository.SaveData data = DataRepository.Load();
        int bestScore = data.BestScore;
        if (_rating > bestScore)
        {
            data.BestScore = _rating;
        }
        DataRepository.Save(data);
        _gameOverPanel.SetActive(true);
        GameOverPanelController gameOverPanelController = _gameOverPanel.GetComponent<GameOverPanelController>();
        gameOverPanelController.setNewScore(_rating);
        gameOverPanelController.setBestScore(bestScore);
    }

    public void BackToMenu()
    {
        _fadeToBlack.GetComponent<FadeToBlack>().StartFadeOut();
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene(_mainSceneName);
        Time.timeScale = 1f;
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
