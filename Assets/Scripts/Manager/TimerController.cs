﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerController : MonoBehaviour
{
    [System.Serializable]
    public class TimeIntCustomEvent : UnityEvent<int> { }
    [System.Serializable]
    public class FreezeCustomEvent : UnityEvent<bool> { }

    private float _remainingTime;

    [Header("Settings")]
    [SerializeField]
    private int _matchDuration = 60;

    [Header("Listener")]
    public TimeIntCustomEvent TimeIntEvent;
    public FreezeCustomEvent FreezeEvent;
    public UnityEvent ElapsedTimeEvent;

    private Coroutine _timerCoroutine;
    private Coroutine _delayCoroutine;

    private void Start()
    {
        _remainingTime = (float)_matchDuration;
        TimeIntEvent?.Invoke((int)(_remainingTime));
        _timerCoroutine = StartCoroutine(TimerCoroutine());
    }

    private IEnumerator TimerCoroutine ()
    {
        FreezeEvent?.Invoke(false);
        float newTime = _remainingTime - Time.deltaTime;
        while (newTime > 0)
        {
            if ((int)(newTime) != (int)(_remainingTime))
            {
                TimeIntEvent?.Invoke((int)(newTime));
            }
            _remainingTime = newTime;
            yield return null;
            newTime = _remainingTime - Time.deltaTime;
        }
        _remainingTime = 0f;
        TimeIntEvent?.Invoke(0);
        ElapsedTimeEvent?.Invoke();
    }

    private IEnumerator DelayCoroutine(int delay)
    {
        FreezeEvent?.Invoke(true);
        float counter = delay;
        while(counter > 0)
        {
            yield return null;
            counter -= Time.deltaTime;
        }
        if (_timerCoroutine != null)
        {
            StopCoroutine(_timerCoroutine);
        }
        _timerCoroutine = StartCoroutine(TimerCoroutine());
    }

    public void FreezeFor(int time)
    {
        if (_delayCoroutine != null)
        {
            StopCoroutine(_delayCoroutine);
        }
        if (_timerCoroutine != null) {
            StopCoroutine(_timerCoroutine);
        }
        _delayCoroutine = StartCoroutine(DelayCoroutine(time));
    }
}
