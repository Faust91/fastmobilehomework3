﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameOverPanelController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _newScore;
    [SerializeField]
    private TextMeshProUGUI _bestScore;

    public void setNewScore(int score)
    {
        _newScore.SetText("" + score);
    }

    public void setBestScore(int score)
    {
        _bestScore.SetText("" + score);
    }
}
