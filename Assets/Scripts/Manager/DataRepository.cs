﻿using System;
using System.IO;
using UnityEngine;

public class DataRepository : MonoBehaviour
{
    private const string _fileLocation = "/savedata.json";
    [System.Serializable]
    public class SaveData
    {
        public int BestScore;
        public int PreferredPowerUp;
    }

    public static bool Save(SaveData data)
    {
        string filePath = Application.persistentDataPath + _fileLocation;
        try
        {
            string savedatajson = JsonUtility.ToJson(data);
            using (StreamWriter sw = new StreamWriter(filePath))
            {
                sw.WriteLine(savedatajson);
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Error occurred while writing data: {e}");
            return false;
        }
        return true;
    }

    public static SaveData Load()
    {
        string filePath = Application.persistentDataPath + _fileLocation;
        string savedatajson = "";
        try
        {
            using (StreamReader sr = new StreamReader(filePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    savedatajson += line;
                }
            }
        }
        catch (FileNotFoundException)
        {
            return new SaveData();
        }
        return savedatajson != "" ? JsonUtility.FromJson<SaveData>(savedatajson) : new SaveData();
    }
}
