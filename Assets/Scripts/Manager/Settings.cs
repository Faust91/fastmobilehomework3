﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    private static Settings _instance;

    [SerializeField]
    private BonusTypeEnum _bonusType = BonusTypeEnum.None;

    public static Settings Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        // Only 1 Settings can exist at a time
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = GetComponent<Settings>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetPowerUp(int value)
    {
        _bonusType = IntToPowerUp(value);
    }

    public BonusTypeEnum GetPowerUp()
    {
        return _bonusType;
    }

    private static BonusTypeEnum IntToPowerUp(int value)
    {
        BonusTypeEnum retVal = BonusTypeEnum.None;
        switch (value)
        {
            case 0:
                retVal = BonusTypeEnum.None;
                break;
            case 1:
                retVal = BonusTypeEnum.Bomb;
                break;
            case 2:
                retVal = BonusTypeEnum.Freeze;
                break;
            default:
                retVal = BonusTypeEnum.None;
                break;
        }
        return retVal;
    }
}
