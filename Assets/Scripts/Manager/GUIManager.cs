﻿using UnityEngine;
using TMPro;

public class GUIManager : MonoBehaviour
{
    private static GUIManager _instance;

    [SerializeField]
    private TextMeshProUGUI _timer;
    [SerializeField]
    private TextMeshProUGUI _rating;

    public static GUIManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        // Only 1 GUI Manager can exist at a time
        if (_instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            _instance = GetComponent<GUIManager>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetRemainingTime(int time)
    {
        _timer.SetText("" + time);
    }

    public void SetRating(int rating)
    {
        _rating.SetText("" + rating);
    }

    public void Freeze(bool freeze)
    {
        _timer.GetComponent<Animator>().SetBool("Freeze", freeze);
    }
}
