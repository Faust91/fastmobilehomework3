﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public class IntCustomEvent : UnityEvent<int> { }

    private static MenuController _instance;

    private int _selectedPowerUp;

    private int _bestScore;

    [SerializeField]
    private GameObject _fadeToBlack;

    [SerializeField]
    private string _gameSceneName = "GameScene";

    public static MenuController Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        // Only 1 GUI Manager can exist at a time
        if (_instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            _instance = GetComponent<MenuController>();
        }
        else
        {
            Destroy(gameObject);
        }

        DataRepository.SaveData data = DataRepository.Load();
        _bestScore = data.BestScore;
        _selectedPowerUp = data.PreferredPowerUp;
        if (_selectedPowerUp < 1)
        {
            _selectedPowerUp = 1;
        }
    }

    public int PowerUp
    {
        get { return _selectedPowerUp; }
        set { _selectedPowerUp = value; }
    }

    public int BestScore
    {
        get { return _bestScore; }
    }

    public void StartGame()
    {
        Settings.Instance.SetPowerUp(_selectedPowerUp);
        DataRepository.SaveData data = DataRepository.Load();
        data.PreferredPowerUp = _selectedPowerUp;
        DataRepository.Save(data);
        _fadeToBlack.GetComponent<FadeToBlack>().StartFadeOut();
    }

    public void LoadGameScene()
    {
        SceneManager.LoadScene(_gameSceneName);
        Time.timeScale = 1f;
    }
}
