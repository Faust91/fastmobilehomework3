﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotController : MonoBehaviour
{
    private Vector2Int _slotPosition;
    private GameObject _tile;

    public Vector2Int SlotPosition
    {
        get { return _slotPosition; }
        set { _slotPosition = value; }
    }

    public GameObject Tile
    {
        get { return _tile; }
        set
        {
            _tile = value;
            if (_tile != null)
            {
                _tile.GetComponent<TileController>().SlotController = this;
                _tile.transform.SetParent(this.transform);
                _tile.transform.SetPositionAndRotation(this.transform.position, this.transform.rotation);
                _tile.transform.localScale = this.transform.localScale;
            }
        }
    }

    public TileController TileController
    {
        get
        {
            if (_tile == null)
            {
                return null;
            }
            else
            {
                return _tile.GetComponent<TileController>();
            }
        }
    }

    public bool DestroyTile()
    {
        bool somethingToDestroy = _tile != null;

        if (somethingToDestroy)
        {
            GameObject.Destroy(_tile);
            this._tile = null;
        }

        return somethingToDestroy;
    }
}
