﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BestScoreFiller : MonoBehaviour
{
    private MenuController _menuController;

    [SerializeField]
    private string _startingString = "Best score: ";

    private void Start()
    {
        _menuController = MenuController.Instance;
        this.gameObject.GetComponent<TextMeshProUGUI>().SetText(_startingString + _menuController.BestScore);
    }



}
