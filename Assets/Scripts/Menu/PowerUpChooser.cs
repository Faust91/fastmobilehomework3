﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PowerUpChooser : MonoBehaviour
{
    public static BonusTypeEnum PowerUpChoosen = BonusTypeEnum.None;

    private MenuController _menuController;

    [SerializeField]
    private Sprite _activeSprite;
    [SerializeField]
    private Sprite _inactiveSprite;

    [SerializeField]
    private BonusTypeEnum _bonusType;

    [SerializeField]
    public UnityEvent SelectEvent;

    private void Awake()
    {
        this.gameObject.GetComponent<Button>().GetComponent<Image>().sprite = _inactiveSprite;
    }

    private void Start()
    {
        _menuController = MenuController.Instance;

        if (PowerUpToInt(_bonusType) == _menuController.PowerUp)
        {
            PowerUpChoosen = _bonusType;
            this.gameObject.GetComponent<Button>().GetComponent<Image>().sprite = _activeSprite;
        }
    }

    public void Select()
    {
        SelectEvent?.Invoke();
        PowerUpChoosen = _bonusType;
        this.gameObject.GetComponent<Button>().GetComponent<Image>().sprite = _activeSprite;
        _menuController.PowerUp = PowerUpToInt(_bonusType);
    }

    public void Deselect()
    {
        PowerUpChoosen = BonusTypeEnum.None;
        this.gameObject.GetComponent<Button>().GetComponent<Image>().sprite = _inactiveSprite;
        _menuController.PowerUp = 0;
    }

    private static int PowerUpToInt(BonusTypeEnum powerUp)
    {
        int retVal = 0;
        switch (powerUp)
        {
            case BonusTypeEnum.None:
                retVal = 0;
                break;
            case BonusTypeEnum.Bomb:
                retVal = 1;
                break;
            case BonusTypeEnum.Freeze:
                retVal = 2;
                break;
            default:
                retVal = 0;
                break;
        }
        return retVal;
    }
}
