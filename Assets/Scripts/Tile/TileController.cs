﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TileController : MonoBehaviour
{
    private Color _selectedColor = new Color(1f, 1f, 1f, 0.3f);
    private Color _deselectedColor = Color.white;

    private int _shapeNum;
    private bool _selected;

    private SlotController _slotController;
    private BoardController _boardController;

    private bool _powerUpActivated;

    private AudioController _audioController;

    private TileEffects _tileEffects;

    public bool PowerUpActivated
    {
        get { return _powerUpActivated; }
        set { _powerUpActivated = value; }
    }

    public SlotController SlotController
    {
        get { return _slotController; }
        set { _slotController = value; }
    }

    public bool Selected
    {
        get { return _selected; }
    }

    public int ShapeNum
    {
        get { return _shapeNum; }
        set
        {
            _shapeNum = value;
            GetComponent<SpriteRenderer>().sprite = ShapeCollector.Instance.GetSprite(_shapeNum);
        }
    }

    public bool IsPowerUp()
    {
        return _shapeNum >= BoardConstants.NumBasicShapes;
    }

    public bool IsABomb()
    {
        return _shapeNum == BoardConstants.BombShapeNum;
    }

    public bool IsAFreeze()
    {
        return _shapeNum == BoardConstants.FreezeShapeNum;
    }

    private void Start()
    {
        _boardController = BoardController.Instance;
        _audioController = AudioController.Instance;
        _tileEffects = GetComponent<TileEffects>();
    }

    public void Select()
    {
        GameObject previouslySelected = BoardController.Instance.SelectedTile;
        if (previouslySelected != null)
        {
            GameObject selected = BoardController.Instance.SelectedTile;
            selected.GetComponent<TileController>().Deselect();
            if (canISwap(previouslySelected))
            {
                _boardController.SwapTiles(gameObject, selected);
                _boardController.StartMatchFindingCoroutine();
                _audioController.PlaySwapSound();
                return; // i don't want to select anything
            }
            else
            {
                previouslySelected.GetComponent<TileController>().Deselect();
            }
        }
        BoardController.Instance.SelectedTile = gameObject;
        _selected = true;
        GetComponent<SpriteRenderer>().color = _selectedColor;
        _audioController.PlaySwapSound();
    }

    private bool canISwap(GameObject previouslySelected)
    {
        Vector2Int myIntPos = _slotController.SlotPosition;
        Vector2Int prevSelPos = previouslySelected.GetComponent<TileController>().SlotController.SlotPosition;

        return (myIntPos.x == prevSelPos.x && (myIntPos.y - prevSelPos.y == 1 || myIntPos.y - prevSelPos.y == -1)) || (myIntPos.y == prevSelPos.y && (myIntPos.x - prevSelPos.x == 1 || myIntPos.x - prevSelPos.x == -1));
    }

    public void Deselect()
    {
        BoardController.Instance.SelectedTile = null;
        _selected = false;
        GetComponent<SpriteRenderer>().color = _deselectedColor;
        _audioController.PlaySwapSound();
    }
}
