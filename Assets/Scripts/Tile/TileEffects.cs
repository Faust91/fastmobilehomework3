﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileEffects : MonoBehaviour
{
    [Header("Effects")]
    [SerializeField] private GameObject _bomb;
    [SerializeField] private GameObject _freeze;
    [SerializeField] private GameObject _puff;

    public void PlayBomb()
    {
        Vector3 spawnPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);
        GameObject bomb = GameObject.Instantiate(_bomb, spawnPosition, this.transform.rotation);
        bomb.GetComponent<ParticleSystem>().Play();
    }

    public void PlayFreeze()
    {
        Vector3 spawnPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);
        GameObject freeze = GameObject.Instantiate(_freeze, spawnPosition, this.transform.rotation);
        freeze.GetComponent<ParticleSystem>().Play();
    }

    public void PlayPuff()
    {
        Vector3 spawnPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1);
        GameObject puff = GameObject.Instantiate(_puff, spawnPosition, this.transform.rotation);
        puff.GetComponent<ParticleSystem>().Play();
    }
}
