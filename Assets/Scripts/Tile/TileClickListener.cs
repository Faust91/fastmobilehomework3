﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileClickListener : MonoBehaviour
{
    private TileController _tileController;

    private BoardController _boardController;

    private void Start()
    {
        _boardController = BoardController.Instance;
        _tileController = GetComponent<TileController>();
    }

    private void OnMouseDown()
    {
        if (_boardController.GameState == GameStateEnum.Idle)
        {
            if (_tileController.Selected)
            {
                _tileController.Deselect();
            }
            else
            {
                _tileController.Select();
            }
        }
    }
}
