﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FadeToBlack : MonoBehaviour
{
    public UnityEvent AfterBlack;

    private const string _startTrigger = "Start";

    [SerializeField]
    private Image _image;

    private Color _black = Color.black;

    [SerializeField]
    private float _duration = 1f;

    public void StartFadeOut()
    {
        Time.timeScale = 0f;
        StartCoroutine(FadeCoroutine());
    }

    private IEnumerator FadeCoroutine()
    {
        float alpha = 0f;
        float elapsedTime = 0f;
        while(elapsedTime < _duration)
        {
            alpha = Mathf.Lerp(0f, 1f, elapsedTime / _duration);
            _image.color = new Color(0f, 0f, 0f, alpha);

            yield return null;
            elapsedTime += Time.unscaledDeltaTime;
        }
        _image.color = _black;
        Callback();
    }

    public void Callback()
    {
        AfterBlack?.Invoke();
    }
}
