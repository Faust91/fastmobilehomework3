﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeCollector : MonoBehaviour
{
    private static ShapeCollector _instance;

    [SerializeField]
    private List<Sprite> _tileSprites;

    public static ShapeCollector Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        // Only 1 GUI Manager can exist at a time
        if (_instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            _instance = GetComponent<ShapeCollector>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Sprite GetSprite(int num)
    {
        return _tileSprites[num];
    }
}
