﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoardController : MonoBehaviour
{
    public static BoardController _instance;

    [Header("Board")]
    [SerializeField]
    private int _width = 8;
    [SerializeField]
    private int _height = 8;
    private const float _xOffset = 0.5f;
    private const float _yOffset = 0.5f;
    private const float _xSize = 1f;
    private const float _ySize = 1f;
    [SerializeField]
    [Range(0, 1)]
    private float _horizontalPercentage = 1f;

    [Header("Prefabs")]
    [SerializeField]
    private GameObject _slotPrefab;
    [SerializeField]
    private GameObject _tilePrefab;
    private const float _slotZPosition = 0f;
    private const float _tileZPosition = 0f;
    private int[] _basicShapes = new int[BoardConstants.NumBasicShapes];

    [Header("PowerUp")]
    [SerializeField]
    private BonusTypeEnum _bonusType = BonusTypeEnum.None;
    [SerializeField]
    private float _powerUpProbability = 0.1f;

    private GameObject[,] _slots;

    private int _matchesStartAt = 3;

    public GameStateEnum GameState;

    private GameObject _selectedTile;

    private float _coroutineDelay = 0.2f;

    private GameManager _gameManager;

    private GameStateEnum _stateBeforePause = GameStateEnum.Idle;
    public void Pause()
    {
        _stateBeforePause = GameState;
        GameState = GameStateEnum.Animating;
    }

    public void Resume()
    {
        GameState = _stateBeforePause;
    }

    [SerializeField]
    private int _freezeTime = 3;

    [Header("Rating")]
    [SerializeField]
    private int _rewardMultiplier = 10;

    [Header("Events")]
    public UnityEvent LineRemovedSoundEvent;
    public UnityEvent ExplosionSoundEvent;
    public UnityEvent FreezeSoundEvent;

    public GameObject SelectedTile
    {
        get { return _selectedTile; }
        set { _selectedTile = value; }
    }

    private int PowerUpShapeNum
    {
        get
        {
            if (_bonusType == BonusTypeEnum.Bomb)
            {
                return BoardConstants.BombShapeNum;
            }
            else if (_bonusType == BonusTypeEnum.Freeze)
            {
                return BoardConstants.FreezeShapeNum;
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }

    public static BoardController Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        // Only 1 Game Manager can exist at a time
        if (_instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            _instance = GetComponent<BoardController>();
        }
        else
        {
            Destroy(gameObject);
        }

        for (int i = 0; i < BoardConstants.NumBasicShapes; i++)
        {
            _basicShapes[i] = i;
        }
        _slots = new GameObject[_width, _height];

        GameState = GameStateEnum.Idle;
    }

    private void Start()
    {
        _gameManager = GameManager.Instance;
        _bonusType = _gameManager.RetrievePowerUp();
        createBoard();
        resizeBoard();
        initializeTiles();
    }

    public void SwapTiles(GameObject tileA, GameObject tileB)
    {
        SlotController slotA = tileA.GetComponent<TileController>().SlotController;
        SlotController slotB = tileB.GetComponent<TileController>().SlotController;

        GameObject tempA = tileA;
        slotA.Tile = tileB;
        slotB.Tile = tempA;
    }

    private void createBoard()
    {
        float startX = transform.position.x;
        float startY = transform.position.y;
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                GameObject slot = Instantiate(_slotPrefab, new Vector3(_xOffset + startX + (_xSize * x), _yOffset + startY + (_ySize * y), _slotZPosition), _slotPrefab.transform.rotation, transform);
                slot.GetComponent<SlotController>().SlotPosition = new Vector2Int(x, y);
                _slots[x, y] = slot;
            }
        }
    }

    private void initializeTiles()
    {
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                GameObject newTile = newRandomTile(x, y, false, true);
                newTile.GetComponent<Animator>().enabled = false;
                _slots[x, y].GetComponent<SlotController>().Tile = newTile;
            }
        }
    }

    private void moveTilesDown()
    {
        bool doItAgain = true;
        while (doItAgain)
        {
            doItAgain = false;
            for (int x = 0; x < _width; x++)
            {
                for (int y = _height - 1; y > 0; y--)
                {
                    if (_slots[x, y].GetComponent<SlotController>().Tile != null && _slots[x, y - 1].GetComponent<SlotController>().Tile == null)
                    {
                        doItAgain = true;
                        _slots[x, y - 1].GetComponent<SlotController>().Tile = newTile(_slots[x, y].GetComponent<SlotController>().TileController.ShapeNum);
                        _slots[x, y].GetComponent<SlotController>().DestroyTile();
                    }
                }
            }
        }
    }

    private void insertTilesInEmptySpaces()
    {
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                if (_slots[x, y].GetComponent<SlotController>().Tile == null)
                {
                    GameObject newTile = newRandomTile(x, y, true, false);
                    _slots[x, y].GetComponent<SlotController>().Tile = newTile;
                }
            }
        }
    }

    private GameObject newTile(int shapeNum)
    {
        GameObject tile = instantiateBlankTile();
        tile.GetComponent<TileController>().ShapeNum = shapeNum;
        return tile;
    }

    private GameObject newRandomTile(int x, int y, bool powerUpEnabled, bool checkNeighborsSimpleShapes)
    {
        GameObject tile = instantiateBlankTile();
        tile.GetComponent<TileController>().ShapeNum = calculateTileShape(x, y, powerUpEnabled, checkNeighborsSimpleShapes);
        return tile;
    }

    private int calculateTileShape(int x, int y, bool powerUpEnabled, bool checkNeighborsSimpleShapes)
    {
        List<int> shapes = new List<int>();
        shapes.AddRange(_basicShapes);
        if (checkNeighborsSimpleShapes)
        {
            if (x > 0 && _slots[x - 1, y].GetComponent<SlotController>().Tile != null)
            {
                int col = _slots[x - 1, y].GetComponent<SlotController>().TileController.ShapeNum;
                if (col < BoardConstants.NumBasicShapes)
                {
                    shapes.Remove(col);
                }
            }
            if (y > 0 && _slots[x, y - 1].GetComponent<SlotController>().Tile != null)
            {
                int col = _slots[x, y - 1].GetComponent<SlotController>().TileController.ShapeNum;
                if (col < BoardConstants.NumBasicShapes)
                {
                    shapes.Remove(col);
                }
            }
            if (x < _width - 1 && _slots[x + 1, y].GetComponent<SlotController>().Tile != null)
            {
                int col = _slots[x + 1, y].GetComponent<SlotController>().TileController.ShapeNum;
                if (col < BoardConstants.NumBasicShapes)
                {
                    shapes.Remove(col);
                }
            }
            if (y < _height - 1 && _slots[x, y + 1].GetComponent<SlotController>().Tile != null)
            {
                int col = _slots[x, y + 1].GetComponent<SlotController>().TileController.ShapeNum;
                if (col < BoardConstants.NumBasicShapes)
                {
                    shapes.Remove(col);
                }
            }
        }
        if (_bonusType != BonusTypeEnum.None && powerUpEnabled)
        {
            float randBonus = UnityEngine.Random.Range(0f, 1f);
            if (randBonus < _powerUpProbability)
            {
                return PowerUpShapeNum;
            }
        }
        int randShapeIndex = UnityEngine.Random.Range(0, shapes.Count);
        return shapes[randShapeIndex];
    }

    private GameObject instantiateBlankTile()
    {
        GameObject tile = Instantiate(_tilePrefab, new Vector3(0, 0, _tileZPosition), _slotPrefab.transform.rotation);
        return tile;
    }

    private void resizeBoard()
    {
        float _screenWidth;
        float _screenHeight;
        // get screen size
        _screenHeight = 2 * Camera.main.orthographicSize;
        _screenWidth = _screenHeight * Camera.main.aspect;
        // scale the board
        float aspect = (float)_width / (float)_height;
        float wantedWidth = _horizontalPercentage * _screenWidth;
        float wantedHeight = wantedWidth / aspect;
        float scale = wantedWidth / _width;
        this.transform.localScale = new Vector2(scale, scale);
        // center the board
        float xOffset = wantedWidth / 2;
        float yOffset = wantedHeight / 2;
        this.transform.position = new Vector2(this.transform.position.x - xOffset, this.transform.position.y - yOffset);
    }

    public void StartMatchFindingCoroutine()
    {
        StartCoroutine(matchingSlotsCoroutine(_slots));
    }

    private IEnumerator matchingSlotsCoroutine(GameObject[,] slots)
    {
        bool explodedAtLeastOnce = false;
        bool freezedAtLeastOne = false;
        List<GameObject> matchingSlots = getMatchingSlots(slots, out explodedAtLeastOnce, out freezedAtLeastOne);
        while (matchingSlots.Count > 0)
        {
            GameState = GameStateEnum.Animating;
            yield return new WaitForSeconds(_coroutineDelay);
            if (explodedAtLeastOnce)
            {
                ExplosionSoundEvent?.Invoke();
                List<GameObject> bombSlots = matchingSlots.FindAll(arg => arg.GetComponent<SlotController>().TileController.IsABomb() && arg.GetComponent<SlotController>().TileController.PowerUpActivated);
                foreach (GameObject bombSlot in bombSlots)
                {
                    bombSlot.GetComponent<SlotController>().Tile.GetComponent<TileEffects>().PlayBomb();
                }
            }
            if (freezedAtLeastOne)
            {
                FreezeSoundEvent?.Invoke();
                List<GameObject> freezeSlots = matchingSlots.FindAll(arg => arg.GetComponent<SlotController>().TileController.IsAFreeze() && arg.GetComponent<SlotController>().TileController.PowerUpActivated);
                foreach (GameObject freezeSlot in freezeSlots)
                {
                    freezeSlot.GetComponent<SlotController>().Tile.GetComponent<TileEffects>().PlayFreeze();
                }
                _gameManager.FreezeTimeFor(_freezeTime);
            }
            if(!explodedAtLeastOnce && !freezedAtLeastOne)
            {
                LineRemovedSoundEvent?.Invoke();
            }
            foreach (GameObject slot in matchingSlots)
            {
                slot.GetComponent<SlotController>().Tile.GetComponent<TileEffects>().PlayPuff();
                slots[slot.GetComponent<SlotController>().SlotPosition.x, slot.GetComponent<SlotController>().SlotPosition.y].GetComponent<SlotController>().DestroyTile();
            }
            _gameManager.IncrementRating(matchingSlots.Count * _rewardMultiplier);
            yield return new WaitForSeconds(_coroutineDelay);
            moveTilesDown();
            insertTilesInEmptySpaces();
            matchingSlots = getMatchingSlots(slots, out explodedAtLeastOnce, out freezedAtLeastOne);

        }
        GameState = GameStateEnum.Idle;
    }

    private List<GameObject> getMatchingSlots(GameObject[,] slots, out bool explodedAtLeastOnce, out bool freezedAtLeastOne)
    {
        List<GameObject> matchingSlots = new List<GameObject>();

        addMatchingSlotsInRows(matchingSlots, slots);
        addMatchingSlotsInCols(matchingSlots, slots);

        bool bombsToCheck = true;
        explodedAtLeastOnce = false;
        freezedAtLeastOne = false;
        while (bombsToCheck)
        {
            bool exploded = false;
            bombsToCheck = activateBombs(matchingSlots, slots, out exploded);
            if (exploded)
            {
                explodedAtLeastOnce = true;
            }
        }
        activateFreezes(matchingSlots, slots, out freezedAtLeastOne);

        return matchingSlots;
    }

    private void activateFreezes(List<GameObject> matchingSlots, GameObject[,] slots, out bool freezed)
    {
        List<GameObject> freezeSlots = matchingSlots.FindAll(arg => arg.GetComponent<SlotController>().TileController.IsAFreeze() && !arg.GetComponent<SlotController>().TileController.PowerUpActivated);
        freezed = freezeSlots.Count > 0;
        foreach (GameObject freezeSlot in freezeSlots)
        {
            freezeSlot.GetComponent<SlotController>().TileController.PowerUpActivated = true;
        }
    }
    private bool activateBombs(List<GameObject> matchingSlots, GameObject[,] slots, out bool exploded)
    {
        bool otherBombs = false;
        List<GameObject> bombSlots = matchingSlots.FindAll(arg => arg.GetComponent<SlotController>().TileController.IsABomb() && !arg.GetComponent<SlotController>().TileController.PowerUpActivated);
        exploded = bombSlots.Count > 0;
        foreach (GameObject bombSlot in bombSlots)
        {
            bombSlot.GetComponent<SlotController>().TileController.PowerUpActivated = true;
            Vector2Int bombPosition = bombSlot.GetComponent<SlotController>().SlotPosition;
            for (int x = bombPosition.x - 1; x <= bombPosition.x + 1; x++)
            {
                for (int y = bombPosition.y - 1; y <= bombPosition.y + 1; y++)
                {
                    if (x >= 0 && y >= 0 && x < _width && y < _height && (x != bombPosition.x || y != bombPosition.y))
                    {
                        if (slots[x, y].GetComponent<SlotController>().TileController.IsABomb() && !slots[x, y].GetComponent<SlotController>().TileController.PowerUpActivated)
                        {
                            otherBombs = true;
                        }
                        addToMatchingIfNotPresent(matchingSlots, slots, x, y);
                    }
                }
            }
        }
        return otherBombs;
    }

    private void addToMatchingIfNotPresent(List<GameObject> matchingSlots, GameObject[,] slots, int x, int y)
    {
        if (!matchingSlots.Contains(slots[x, y]))
        {
            matchingSlots.Add(slots[x, y]);
        }
    }

    private void addMatchingSlotsInCols(List<GameObject> matchingSlots, GameObject[,] slots)
    {
        for (int x = 0; x < _width; x++)
        {
            List<GameObject> tempSlots = new List<GameObject>();
            int tempShape = -1;
            int counter = 0;
            for (int y = 0; y < _height; y++)
            {
                if (slots[x, y].GetComponent<SlotController>().Tile != null
                    && (tempShape == -1
                    || slots[x, y].GetComponent<SlotController>().TileController.ShapeNum == tempShape
                    || slots[x, y].GetComponent<SlotController>().TileController.IsPowerUp()))
                {
                    // 2-tiles match or powerUp
                    if (slots[x, y].GetComponent<SlotController>().TileController.ShapeNum < BoardConstants.NumBasicShapes)
                    {
                        tempShape = slots[x, y].GetComponent<SlotController>().TileController.ShapeNum;
                    }
                }
                else
                {
                    if (counter >= _matchesStartAt)
                    {
                        matchingSlots.AddRange(tempSlots);
                    }
                    tempShape = slots[x, y].GetComponent<SlotController>().TileController.ShapeNum;
                    counter = 0;
                    tempSlots.Clear();
                    for (int yi = y - 1; yi >= 0 && slots[x, yi].GetComponent<SlotController>().TileController.IsPowerUp(); yi--)
                    {
                        counter++;
                        if (!matchingSlots.Contains(slots[x, yi]))
                        {
                            tempSlots.Add(slots[x, yi]);
                        }
                    }
                }
                counter++;
                if (!matchingSlots.Contains(slots[x, y]))
                {
                    tempSlots.Add(slots[x, y]);
                }
                if (y == _height - 1 && counter >= _matchesStartAt)
                {
                    matchingSlots.AddRange(tempSlots);
                }
            }

        }
    }

    private void addMatchingSlotsInRows(List<GameObject> matchingSlots, GameObject[,] slots)
    {
        for (int y = 0; y < _height; y++)
        {
            List<GameObject> tempSlots = new List<GameObject>();
            int tempShape = -1;
            int counter = 0;
            for (int x = 0; x < _width; x++)
            {
                if (slots[x, y].GetComponent<SlotController>().Tile != null
                    && (tempShape == -1
                    || slots[x, y].GetComponent<SlotController>().TileController.ShapeNum == tempShape
                    || slots[x, y].GetComponent<SlotController>().TileController.IsPowerUp()))
                {
                    // 2-tiles match or powerUp

                    if (slots[x, y].GetComponent<SlotController>().TileController.ShapeNum < BoardConstants.NumBasicShapes)
                    {
                        tempShape = slots[x, y].GetComponent<SlotController>().TileController.ShapeNum;
                    }
                }
                else
                {
                    if (counter >= _matchesStartAt)
                    {
                        matchingSlots.AddRange(tempSlots);
                    }
                    tempShape = slots[x, y].GetComponent<SlotController>().TileController.ShapeNum;
                    counter = 0;
                    tempSlots.Clear();
                    for (int xi = x - 1; xi >= 0 && slots[xi, y].GetComponent<SlotController>().TileController.IsPowerUp(); xi--)
                    {
                        counter++;
                        if (!matchingSlots.Contains(slots[xi, y]))
                        {
                            tempSlots.Add(slots[xi, y]);
                        }
                    }
                }
                counter++;
                if (!matchingSlots.Contains(slots[x, y]))
                {
                    tempSlots.Add(slots[x, y]);
                }
                if (x == _width - 1 && counter >= _matchesStartAt)
                {
                    matchingSlots.AddRange(tempSlots);
                }
            }

        }
    }

}
